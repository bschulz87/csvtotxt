'use strict';

const fs = require('fs')
const csv = require('csvtojson')
const _ = require('underscore')
const moment = require('moment')

const wochentage = ['Monday','Tuesday','Wednesday','Thursday','Friday']

const formatWorklog = (file, worklog) => {

    const output = []
    const wochen = _.uniq(worklog, 'woche')

    wochen.forEach(function(woche) {
      output.push('KW ' + woche.woche)
      const filterWorklogWoche = _.filter(worklog, function(item){
        return item.woche == woche.woche
      })
      wochentage.forEach(function(tag){
        const filterWorklogTag = _.filter(filterWorklogWoche, function(item){
          return item.wochentag == tag
        })
        const datum = _.uniq(filterWorklogTag, 'datum')
        output.push(tag)
        if(filterWorklogTag.length > 0){
          filterWorklogTag.forEach(function(eintrag){
            output.push(eintrag.von + '-' + eintrag.bis + ': ' + eintrag.kunde + ' ' + eintrag.projekt + ' ' + eintrag.taetigkeit)
          })
        }
      })

    })

  const final = JSON.stringify(output, null, 4)
  require('fs').writeFile(
      './dist/'+file+'.txt',
      final,
      function (err) {
          if (err) {
              console.log('Konnte nicht speichern');
          }
      }
  )

}


fs.readdir('./src', function(err, items) {
  items.forEach(function(item){
    const worklog = []
    csv()
    .fromFile('./src/'+item)
    .on('json',(jsonObj)=>{
      let datum = jsonObj.Datum+'2017'
      let woche = moment(datum, 'DD.MM.').week()
      let wochentag = moment(datum, 'DD.MM.').format('dddd')
      worklog.push({
        datum,
        woche,
        wochentag,
        von: jsonObj.von,
        bis: jsonObj.bis,
        kunde: jsonObj.Kunde,
        projekt: jsonObj.Projekt,
        taetigkeit: jsonObj.Tätigkeit
      })
    })
    .on('done',(error)=>{
      formatWorklog(item, worklog)
    })
  })
})
